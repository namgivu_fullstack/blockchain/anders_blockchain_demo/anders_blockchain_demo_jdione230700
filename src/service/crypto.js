export let genKeyPair = () => {
  const privateKey = (Math.random() * 16).toString(16).replace('.','')  // ref. https://stackoverflow.com/a/58326357/248616
  const publicKey  = (Math.random() * 16).toString(16).replace('.','')

  return [privateKey, publicKey]
}

export let getPublicKey = (prvKey) => {
  return (Math.random() * 16).toString(16).replace('.','')
}
