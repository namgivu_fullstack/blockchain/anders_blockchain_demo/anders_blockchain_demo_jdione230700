import React, { useState } from "react";
import {
  Card,
  CardHeader,
  Heading,
  CardBody,
  InputGroup,
  InputRightAddon,
  Input,
} from "@chakra-ui/react";

import {genKeyPairs, getPublicKey} from "../../service/crypto"


export default function Index() {
  const [privetKey, setPrivateKey] = useState("");
  const [publicKey, setPublicKey] = useState("");

  const onClickRandomButton = () => {
    const [_privateKey, _publicKey] = genKeyPairs();
    setPrivateKey(_privateKey);
    setPublicKey(_publicKey);
  };

  const onChangePrivateKey = (e) => {
    let savePublicKey = getPubKey(e.target.value)
    setPublicKey(savePublicKey)
    setPrivateKey(e.target.value)
  }

  return (
    <div className="">
      <div className="pt-8 flex justify-center">
        <Card width={1200} height={300}>
          <CardHeader bgColor="#edf1f7">
            <Heading size="lg" fontWeight={400}>
              Public / Private Key Pairs
            </Heading>
          </CardHeader>
          <CardBody className="space-y-4">
            <div className="text-black text-base">Private Key</div>
            <InputGroup size="md">
              <Input value={privetKey} onChange={onChangePrivateKey} />
              <InputRightAddon
                children="Random"
                bgColor="#727b84"
                textColor="white"
                onClick={onClickRandomButton}
              />
            </InputGroup>

            <div>Public Key</div>
            <InputGroup size="md" disabled>
              <Input disabled bgColor="#edf1f7" value={publicKey} />
            </InputGroup>
          </CardBody>
        </Card>
      </div>
    </div>
  );
}
